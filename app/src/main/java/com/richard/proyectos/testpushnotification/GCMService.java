package com.richard.proyectos.testpushnotification;

import android.app.IntentService;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

/**
 * Created by ricoru on 15/12/15.
 */
public class GCMService extends IntentService {

    private NotificationManager mNotificationManager;

    private int NOTIFICATION_ID = 1234;

    public GCMService() {
        super("Listener-Service");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        Bundle extras = intent.getExtras();
        Log.i("INFO",extras.toString());
        /*Log.i("INFO",extras.getString("title"));
        Log.i("INFO", extras.getString("body"));*/
        //sendNotification("Mensaje ",extras.toString());
        sendNotification(extras.getString("title"),extras.getString("message"));

        GCMBroadCastReceiver.completeWakefulIntent(intent);
    }

    private void sendNotification(String title,String msg) {
        mNotificationManager = (NotificationManager)this.getSystemService(Context.NOTIFICATION_SERVICE);

        PendingIntent contentIntent = PendingIntent.getActivity(this, 0,new Intent(this, MainActivity.class), 0);

        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(this)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setContentTitle(title)
                .setStyle(new NotificationCompat.BigTextStyle()
                        .bigText(msg))
                .setContentText(msg);

        mBuilder.setContentIntent(contentIntent);
        mNotificationManager.notify(NOTIFICATION_ID, mBuilder.build());
    }
}